#include <string>
#include <iostream>
#include <time.h>
#include <conio.h>

using namespace std;

void sort(int numb[], bool asc, bool *sorted);
void add(int numb[]);
void print(int numb[]);
void swap(int first, int second, int numb[]);
int search(int numb[]);

#define getch() _getch();

int main()
{
	srand(time(NULL));

	int numbers[10];
	bool ascend;
	bool sorted = false;

	add(numbers);

	while (true)
	{
		cout << "[x] Ascending\t[z] Descending" << endl;

		char input = getch();
		if (input == 120)//x
		{
			ascend = true;
			break;
		}
		else if (input == 122)//z
		{
			ascend = false;
			break;
		}
		system("CLS");
	}

	while (!sorted)
	{
		sort(numbers, ascend, &sorted);
	}
	print(numbers);
	cout << search(numbers) << " match(es) has been found on the particular number." << endl;
	system("pause");
} 

void add(int numb[])
{
	for (size_t i = 0; i < 10; i++)
	{
		int random = rand() % 69 + 1;
		numb[i] = random;
	}
}

void sort(int numb[], bool asc, bool *sorted)
{
	bool sort = true;

	for (size_t i = 0; i < 9; i++)
	{
		if (numb[i] > numb[i + 1] && asc)
		{
			swap(i, i + 1, numb);
			sort = false;
		}
		else if (numb[i] < numb[i + 1] && !asc)
		{
			swap(i, i + 1, numb);
			sort = false;
		}
	}

	if (sort != false)
	{
		(*sorted) = true;
	}
}

void swap(int first, int second, int numb[])
{
	int tmp = numb[first];
	numb[first] = numb[second];
	numb[second] = tmp;
}

void print(int numb[])
{
	for (size_t i = 0; i < 10; i++)
	{
		cout << numb[i] << ", ";
	}
}

int search(int numb[])
{
	int searching;
	cout << endl << "What number should we look for? ";
	cin >> searching;
	int count = 0;

	for (size_t i = 0; i < 10; i++)
	{
		if (numb[i] == searching)
		{
			count++;
		}
	}

	return count;
}