#include <iostream>
#include <string>

using namespace std;

void computeSum(int input, int sum)
{
	if (sum == 0)
	{
		cout << "Compute the sum of digits of a number\nInput: " << input << endl;
	}
	string digit = to_string(input);
	cout << digit.at(digit.length()-1);
	sum += input % 10;
	input /= 10;
	if (input <= 0)
	{
		cout << " = " << sum << endl;
		cout << "Output: " << sum << endl << endl;
		return;
	}
	cout << " + ";
	computeSum(input, sum);
}

void fibonacci(int n, int prev, int curr)
{
	if (prev == 0)
	{
		cout << "Fibonacci Sequence\nInput: " << n << endl << "Output: ";
	}
	cout << curr;
	if (n <= curr)
	{
		cout << endl;
		return;
	}
	int tmp = curr;
	curr = prev + curr;
	prev = tmp;
	cout << ", ";
	fibonacci(n, prev, curr);
}

void prime(int input, int divisor, int factors)
{
	if (divisor == 2)
	{
		cout << endl << "Check a number if it is prime or not\nInput: " << input << endl;
	}
	if (input % divisor == 0)
	{
		factors++;
	}
	divisor++;

	if (divisor >= input)
	{
		if (factors > 1 || input < 2)
		{
			cout << "Output: " << input << " is not a prime number." << endl;
		}
		else if (factors < 2)
		{
			cout << "Output: " << input << " is a prime number." << endl;
		}
		return;
	}

	prime(input, divisor, factors);
}

int main()
{
	int input;
	cout << "Input for 'Computing The Sum Of Digits': ";
	cin >> input;
	computeSum(input, 0);
	cout << "Input for 'Fibonacci': ";
	cin >> input;
	fibonacci(input, 0, 1);
	cout << endl;
	cout << "Input for 'Prime Checking': ";
	cin >> input;
	prime(input, 2, 0);
	system("pause");
}