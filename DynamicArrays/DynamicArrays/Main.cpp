#include <string>
#include <iostream>
#include "UnorderedArray.h"
#include <time.h>

using namespace std;

int main()
{
	srand(time(NULL));
	int input;

	cout << "Size of your Unordered Array: ";
	cin >> input;

	UnorderedArray<int> numbers(input);

	for (size_t i = 0; i < input; i++)
	{
		numbers.push(rand() % 100+1);
	}

	for (int i = 0; i < numbers.getSize(); i++)
	{
		cout << numbers[i];
		if (i < numbers.getSize() - 1)
		{
			cout << ", ";
		}
	}
	cout << endl;

	cout << "Give the index of the element you want to delete: ";
	cin >> input;
	numbers.remove(input);

	for (int i = 0; i < numbers.getSize(); i++)
	{
		cout << numbers[i];
		if (i < numbers.getSize() - 1)
		{
			cout << ", ";
		}
	}
	cout << endl;

	cout << "Search for: ";
	cin >> input;
	cout << "Result = " << numbers.search(input) << endl;

	system("pause");
}