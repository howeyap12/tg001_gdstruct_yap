#include <string>
#include <iostream>
#include "OrderedArray.h"
#include "UnorderedArray.h"
#include <time.h>

using namespace std;

int main()
{
	srand(time(NULL));

	int size;
	cout << "Enter the size for your Dynamic Array: ";
	cin >> size;
	
	OrderedArray<int> ordered(size);
	UnorderedArray<int> unordered(size);

	for (int i = 0; i < size; i++)
	{
		int randNum = rand() % 100 + 1;
		ordered.push(randNum);
		unordered.push(randNum);
	}

	while (true)
	{
		system("CLS");
		cout << "Unordered Contents: ";
		for (size_t i = 0; i < unordered.getSize(); i++)
		{
			cout << unordered[i] << " ";
		}

		cout << endl << "Ordered Contents: ";
		for (size_t i = 0; i < ordered.getSize(); i++)
		{
			cout << ordered[i] << " ";
		}

		int input;
		int comparisons = 0;

		cout << endl<< endl << "What do you want to do?" << endl;
		cout << "1 - Remove an element using index" << endl;
		cout << "2 - Search" << endl;
		cout << "3 - Expand and generate random values" << endl;

		cin >> input;
		switch (input)
		{
		case 1:
			cout << endl << "Enter the index of the element you want to remove: ";
			cin >> input;
			ordered.remove(input);
			unordered.remove(input);
			cout << endl << "Element removed: " << input << endl;
			break;
		case 2:
			cout << endl << "Input element to search: ";
			cin >> input;

			if (unordered.linearSearch(input, &comparisons) == -1)
			{
				cout << "Element " << input << " not found." << endl;
			}
			else
			{
				cout << "Linear search took " << comparisons << " comparisons." << endl;
			}

			comparisons = 0;
			if (ordered.binarySearch(input, &comparisons) == -1)
			{
				cout << "Element " << input << " not found." << endl;
			}
			else
			{
				cout << "Binary search took " << comparisons << " comparisons." << endl;
			}

			if (unordered.linearSearch(input, &comparisons) != -1 && ordered.binarySearch(input, &comparisons) != -1)
			{
				cout << "Element " << input << " found. Index " << unordered.linearSearch(input, &comparisons);
				cout << " for Linear Search, index " << ordered.binarySearch(input, &comparisons) << " for binary search" << endl;
			}
			break;
		case 3:
			cout << "Input size of expansion: ";
			cin >> input;
			for (int i = 0; i < input; i++)
			{
				int randNum = rand() % 100 + 1;
				ordered.push(randNum);
				unordered.push(randNum);
			}
			cout << "Arrays have been expanded!" << endl;
			break;
		default:
			break;
		}

		system("pause");
	}
}
