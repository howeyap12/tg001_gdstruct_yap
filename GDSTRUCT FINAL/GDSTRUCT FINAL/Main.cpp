#include <string>
#include <iostream>
#include "UnorderedArray.h"
#include <time.h>

using namespace std;

int main()
{
	srand(time(NULL));

	UnorderedArray<int> queue(1);
	UnorderedArray<int> stack(1);

	while (true)
	{
		//Printing values in for loop
		cout << "Queue: ";
		for (size_t i = 0; i < queue.getSize(); i++)
		{
			cout << queue[i] << " ";
		}
		cout << endl << "Stack: ";
		for (size_t i = 0; i < stack.getSize(); i++)
		{
			cout << stack[i] << " ";
		}

		//asking for user input
		int choice;

		cout << endl << "What do you want to do?" << endl;
		cout << "1 - Push elements" << endl;
		cout << "2 - Pop elements" << endl;
		cout << "3 - Print everything then empty set" << endl;

		cin >> choice;
		switch (choice)
		{
		case 1: // push
			int newVal;
			cout << endl << "Enter a number: ";
			cin >> newVal;
			queue.push(newVal);
			stack.pushFront(newVal);
			break;
		case 2: // pop
			cout << endl << "You have popped the front elements." << endl;
			queue.popFront();
			stack.popFront();
			break;
		case 3: // print and empty
			cout << "Queue elements: " << endl;
			for (size_t i = 0; i < queue.getSize(); i++)
			{
				cout << queue[i] << endl;
			}
			cout << "Stack elements: " << endl;
			for (size_t i = 0; i < stack.getSize(); i++)
			{
				cout << stack[i] << endl;
			}
			queue.clear();
			stack.clear();
			break;
		}

		if (queue.getSize() > 0 && stack.getSize() > 0)
		{
			cout << endl << "Top element of sets: " << endl; // printing top value of each sets
			cout << "Queue: " << queue.top() << endl;
			cout << "Stack: " << stack.top() << endl;
		}
		system("pause");
		system("CLS");
	}
}

