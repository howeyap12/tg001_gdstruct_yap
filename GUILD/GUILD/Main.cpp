#include <string>
#include <iostream>
#include <conio.h>

using namespace std;

void initName(int size, string* guild)
{
	for (size_t i = 0; i < size; i++)
	{
		cout << "What is member no. " << i+1 << "'s name: ";
		cin >> guild[i];
	}
}

void print(int size, string* guild, string name)
{
	cout << name <<" members: " << endl << endl;

	for (size_t i = 0; i < size; i++)
	{
		cout << guild[i] << endl;
	}
	cout << endl << "===============" << endl << endl;
	cout << "[R] Rename" << endl << "[A] Add" << endl << "[D] Delete" << endl << endl;
}

void rename(int size, string* guild)
{
	string input;
	cout << "Rename who? ";
	cin >> input;

	for (size_t i = 0; i < size; i++)
	{
		if (input == guild[i])
		{
			cout << guild[i] << "'s new name: ";
			cin >> input;
			guild[i] = input;
		}
	}
}

void add(int size, string** guild)
{
	string* tmp = new string[size];

	for (size_t i = 0; i < size-1; i++)
	{
		tmp[i] = (*guild)[i];
	}
	string name;
	cout << "What's this new member's name? ";
	cin >> name;

	(*guild) = tmp;
	(*guild)[size - 1] = name;
}

void del(int size, string** guild)
{
	string* tmp = new string[size];

	string name;
	cout << "Remove who? ";
	cin >> name;

	for (size_t i = 0; i < size; i++)
	{
		if (name == (*guild)[i])
		{
			tmp[i] = (*guild)[i + 1];
			name = (*guild)[i + 1];
		}
		else
		{
			tmp[i] = (*guild)[i];
		}
	}

	(*guild) = tmp;
}

int main()
{
	string name;
	cout << "Please input the guild name: ";
	cin >> name;

	int size;
	cout << "Please input the initial size for the guild: ";
	cin >> size;

	string *guild = new string[size];
	initName(size, guild);

	while (true)
	{
		system("CLS");
		print(size, guild, name);

		char input = _getch();
		if (input == 114)//r
		{
			rename(size, guild);
		}
		else if (input == 97)//a
		{
			size++;
			add(size, &guild);
		}
		else if (input == 100)//d
		{
			size--;
			del(size, &guild);
		}
		system("pause");
	}

	delete[] guild;
}
